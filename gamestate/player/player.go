package player

import "poker/cards"

// Player is a player.
type Player struct {
	Chips           float64
	Folded          bool
	CheckedorCalled bool
	Raised          bool
	Cards           []cards.Card
	Name            string
	TypeOfPlayer    string
	MoneyInPot      float64
	NoAllowedAction bool
}
