package gamestate

import (
	"fmt"
	"poker/cards"
)

var river0 = cards.Card{Value: 9, Suit: "c"}

func (gamestate *GameState) DealRiver() {

	gamestate.ActionTracker.NbrBets = 0 // Reset the betting
	gamestate.ActionTracker.CurrentTotalBet = 0

	// Reset player betting variables.
	for i := 0; i < len(gamestate.Players); i++ {
		gamestate.Players[i].CheckedorCalled = false
		gamestate.Players[i].Raised = false
		gamestate.Players[i].MoneyInPot = 0
		if !(gamestate.Players[i].Folded) {
			gamestate.Players[i].NoAllowedAction = false
		}
	}

	gamestate.Board = append(gamestate.Board, gamestate.Deck.Deck[0])
	// gamestate.Board = append(gamestate.Board, river0)
	gamestate.Deck.Deck = gamestate.Deck.Deck[1:]

	fmt.Println(fmt.Sprintf("*** RIVER *** [%s %s %s] [%s] [%s]", cards.ReturnIndividualCardasString(gamestate.Board[0]), cards.ReturnIndividualCardasString(gamestate.Board[1]), cards.ReturnIndividualCardasString(gamestate.Board[2]), cards.ReturnIndividualCardasString(gamestate.Board[3]), cards.ReturnIndividualCardasString(gamestate.Board[4])))

	endRiverAction := false
	for !endRiverAction {

		for j := 0; j < len(gamestate.Players); j++ {

			if gamestate.Players[gamestate.ActionTracker.CurrentActorPosition].Folded == false {
				gamestate = PlayerAction(gamestate)

			}
			test, y := checkIfHandOverFromFolding(gamestate.Players)

			if test {
				gamestate.AwardWinnerFromFolds(y)
				endRiverAction = true
				gamestate.HandIsOver = true
				break
			}
			counter := 0
			for i := 0; i < len(gamestate.Players); i++ {
				if gamestate.Players[gamestate.ActionTracker.CurrentActorPosition].Folded == true || gamestate.Players[gamestate.ActionTracker.CurrentActorPosition].NoAllowedAction == true {
					counter++
				}
			}

			if counter == len(gamestate.Players) {
				endRiverAction = true
				break
			}

			if j == len(gamestate.Players)-1 {
				j = -1
			}

		}

	}
}
