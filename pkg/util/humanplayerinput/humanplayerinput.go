package humanplayerinput

import (
	"fmt"

	"github.com/manifoldco/promptui"
)

// pass in available options
func ReadHumanPlayerInput(name string, o []string) string {

	prompt := promptui.Select{
		Label: "Select Option",
		Items: o,
	}

	_, result, err := prompt.Run()

	if err != nil {
		fmt.Printf("Prompt failed %v\n", err)
		return ""
	}

	return result
}
