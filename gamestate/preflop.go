package gamestate

import (
	"fmt"
	"log"
	"math/rand"
	"poker/cards"
	"time"
)

// PreFlop is pre flop.
type Action struct {
	NbrBets              int
	CurrentTotalBet      float64
	CurrentActorPosition int
	Multiplier           int
}

// var player0card0 = cards.Card{Value: 3, Suit: "h"}
// var player0card1 = cards.Card{Value: 11, Suit: "s"}

// var player1card0 = cards.Card{Value: 3, Suit: "d"}
// var player1card1 = cards.Card{Value: 8, Suit: "h"}

// PreFlop is preflop.
func (gamestate *GameState) PreFlop() {
	gamestate.ActionTracker = Action{NbrBets: 1, CurrentTotalBet: 0.0, Multiplier: 1}
	gamestate.AssignInitialPositions() // Randomizes positions, should only happen on the first hand
	gamestate.ApplyBlinds()
	gamestate.DealHoleCards()
	// Assign the dealer position
	// Figure out how to wrap the arrays w.r.t. whose turn it is

	endPFAction := false
	for !endPFAction {

		for j := 0; j < len(gamestate.Players); j++ {

			if gamestate.Players[gamestate.ActionTracker.CurrentActorPosition].Folded == false {
				gamestate = PlayerAction(gamestate)

			}
			test, y := checkIfHandOverFromFolding(gamestate.Players)

			if test {
				// log.Println("All other players folded, winner is: ", gamestate.Players[y].Name)
				gamestate.AwardWinnerFromFolds(y)
				endPFAction = true
				gamestate.HandIsOver = true
				break
			}
			counter := 0
			for i := 0; i < len(gamestate.Players); i++ {
				if gamestate.Players[gamestate.ActionTracker.CurrentActorPosition].Folded == true || gamestate.Players[gamestate.ActionTracker.CurrentActorPosition].NoAllowedAction == true {
					counter++
				}
			}

			if counter == len(gamestate.Players) {
				endPFAction = true
				break
			}

			if j == len(gamestate.Players)-1 {
				j = -1
			}

		}

	}
}

// DeterminePreFlopActor is the preflop actor.
// Position 0 is the small blind
// If 2 handed, the small blind is also the dealer, and the small blind acts first preflop
// If 3 Handed, UTG acts first preflop, acts last after the flop
func DeterminePreFlopActor(nbr int) int {
	// If there are only two players, the small blind acts first. Otherwise it is UTG.
	if nbr == 2 {
		return 0
	}
	return 2
}

// AssignInitialPositions assigns the positions.
func (gamestate *GameState) AssignInitialPositions() {
	rand.Seed(time.Now().UnixNano())
	rand.Shuffle(len(gamestate.Players), func(i, j int) {
		gamestate.Players[i], gamestate.Players[j] = gamestate.Players[j], gamestate.Players[i]
	})

}

// ApplyBlinds applies the blinds.
func (gamestate *GameState) ApplyBlinds() {
	gamestate.Players[0].Chips -= float64(gamestate.BigBlindSize / 2)
	gamestate.Players[0].MoneyInPot += float64(gamestate.BigBlindSize / 2)
	gamestate.Pot[0].Size += float64(gamestate.BigBlindSize / 2)
	log.Println(gamestate.Players[0].Name, "posts the small blind.")
	gamestate.Players[1].Chips -= float64(gamestate.BigBlindSize)
	gamestate.Players[1].MoneyInPot += float64(gamestate.BigBlindSize)
	log.Println(gamestate.Players[1].Name, "posts the big blind.")
	gamestate.Pot[0].Size += float64(gamestate.BigBlindSize)
	gamestate.ActionTracker.CurrentTotalBet = gamestate.ActionTracker.CurrentTotalBet + float64(gamestate.BigBlindSize)
	pfActor := DeterminePreFlopActor(len(gamestate.Players))
	gamestate.ActionTracker.CurrentActorPosition = pfActor

}

// Deal does a deal.
func (gamestate *GameState) DealHoleCards() {
	for j := 0; j < 2; j++ {
		for i := 0; i < len(gamestate.Players); i++ {
			gamestate.Players[i].Cards = append(gamestate.Players[i].Cards, gamestate.Deck.Deck[0])
			gamestate.Deck.Deck = gamestate.Deck.Deck[1:]
		}
	}

	//Remove this code, for hardcoding cards
	// gamestate.Players[0].Cards = append(gamestate.Players[0].Cards, player0card0)
	// gamestate.Players[0].Cards = append(gamestate.Players[0].Cards, player0card1)
	// gamestate.Players[1].Cards = append(gamestate.Players[1].Cards, player1card0)
	// gamestate.Players[1].Cards = append(gamestate.Players[1].Cards, player1card1)
	// log.Println(gamestate.Players[0].Cards)
	log.Println("*** HOLE CARDS ***")

	player0Cards := cards.ReturnCardAsString(gamestate.Players[0].Cards)
	player0CardsString := fmt.Sprintf("[ %s %s ]", player0Cards[0], player0Cards[1])
	player1Cards := cards.ReturnCardAsString(gamestate.Players[1].Cards)
	player1CardsString := fmt.Sprintf("[ %s %s ]", player1Cards[0], player1Cards[1])

	log.Println("Dealt to", gamestate.Players[0].Name, player0CardsString)
	log.Println("Dealt to", gamestate.Players[1].Name, player1CardsString)

}
