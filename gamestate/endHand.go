package gamestate

import (
	"log"
	"poker/cards"
)

func (gamestate *GameState) AwardWinnerFromFolds(positionOfWinner int) {
	log.Println("Player", positionOfWinner, " wins ", gamestate.Pot[0].Size, "chips")
	gamestate.Players[positionOfWinner].Chips += gamestate.Pot[0].Size
	gamestate.HandIsOver = true
}

func (gamestate *GameState) AwardWinner() {

	if len(gamestate.Pot[0].EligibleWinners) == 1 {
		positionOfWinner := ReturnPlayerPositionUsingPlayerName(gamestate, gamestate.Pot[0].EligibleWinners[0])
		gamestate.AwardWinnerFromFolds(positionOfWinner)
		return
	}

	for j := 0; j < len(gamestate.Board); j++ {
		gamestate.Players[0].Cards = append(gamestate.Players[0].Cards, gamestate.Board[j])
		gamestate.Players[1].Cards = append(gamestate.Players[1].Cards, gamestate.Board[j])
	}
	var hand0 []cards.AllPossibleHands
	var hand1 []cards.AllPossibleHands

	hand0 = cards.GenerateAllPossible5HandCombos(gamestate.Players[0].Cards)
	hand1 = cards.GenerateAllPossible5HandCombos(gamestate.Players[1].Cards)
	maxhand0, counter0 := CalculateMaxHandValue(hand0)
	maxhand1, counter1 := CalculateMaxHandValue(hand1)

	var maxhands0 []cards.AllPossibleHands
	if counter0 > 1 {
		for i := 0; i < len(hand0); i++ {
			if hand0[i].HandValue == maxhand0 {
				maxhands0 = append(maxhands0, hand0[i])

			}
		}
	}

	var maxhands1 []cards.AllPossibleHands
	if counter1 > 1 {
		for i := 0; i < len(hand1); i++ {
			if hand1[i].HandValue == maxhand1 {
				maxhands1 = append(maxhands1, cards.AllPossibleHands{maxhand1, hand1[i].IndividualHand})

			}
		}
	}

	currentBestHandIndex0 := 0
	for i := 0; i < len(maxhands0); i++ {
		updateIndex := cards.TieBreaker(maxhands0[currentBestHandIndex0], maxhands0[i])
		if updateIndex == 1 {
			currentBestHandIndex0 = i
		}
	}

	currentBestHandIndex1 := 0
	for i := 0; i < len(maxhands1); i++ {
		updateIndex := cards.TieBreaker(maxhands1[currentBestHandIndex1], maxhands1[i])
		if updateIndex == 1 {
			currentBestHandIndex1 = i
		}
	}

	// If the counter is greater than zero, this means that there is more than one hand with the same
	// max value. These hands need to be compared in order to submit one max hand to compare against all winners.

	if maxhand0 > maxhand1 {
		log.Println("Player 0 wins with", cards.HandValueAsString(maxhand0))
		log.Println("Player 0 wins ", gamestate.Pot[0].Size, "chips")
		gamestate.Players[0].Chips += gamestate.Pot[0].Size
	} else if maxhand0 == maxhand1 {

		tiebreaker := cards.TieBreaker(maxhands0[currentBestHandIndex0], maxhands1[currentBestHandIndex1])

		if tiebreaker == 0 {
			log.Println("Player 0 wins with", cards.HandValueAsString(maxhand0))
			log.Println("Player 0 wins ", gamestate.Pot[0].Size, "chips")
			gamestate.Players[0].Chips += gamestate.Pot[0].Size
		} else if tiebreaker == 1 {
			log.Println("Player 1 wins with", cards.HandValueAsString(maxhand1))
			log.Println("Player 1 wins ", gamestate.Pot[0].Size, "chips")
			gamestate.Players[1].Chips += gamestate.Pot[0].Size
		} else {
			log.Println("Pot is split")
			log.Println("Players split pot with", cards.HandValueAsString(maxhand1))
			splitPot := gamestate.Pot[0].Size / 2
			gamestate.Players[0].Chips += splitPot
			gamestate.Players[1].Chips += splitPot
		}
	} else {
		log.Println("Player 1 wins with", cards.HandValueAsString(maxhand1))
		log.Println("Player 1 wins ", gamestate.Pot[0].Size, "chips")
		gamestate.Players[1].Chips += gamestate.Pot[0].Size
	}

	gamestate.HandIsOver = true

}

func ReturnPlayerPositionUsingPlayerName(gamestate *GameState, playerName string) int {
	for i := 0; i < len(gamestate.Players); i++ {
		if gamestate.Players[i].Name == playerName {
			return i
		}

	}
	return -1
}

func CalculateMaxHandValue(hand []cards.AllPossibleHands) (int, int) {
	maxhand := 0

	for i, _ := range hand {
		hand[i].HandValue = hand[i].IndividualHand.Value()
		if hand[i].HandValue > maxhand {
			maxhand = hand[i].HandValue
		}
	}

	counter := 0
	for i, _ := range hand {
		if maxhand == hand[i].HandValue {
			counter += 1
		}

	}
	return maxhand, counter
}

// func CalculateTieBreakerBetweenTwoHands(maxhandvalue int, hand []cards.AllPossibleHands) int {
// 	bestHandIndex := 0

// 	for i := 0; i < len(hand); i++ {
// 		if hand[i].HandValue == maxhandvalue {
// 			bestHandIndex = i
// 			break
// 		}
// 	}

// 	// Compare the value of the hand at the previous besthandindex with the next value
// 	// Where the handvalues equal one another
// 	// for i := bestHandIndex; i < len(hand); i++ {
// 	// 	if hand[bestHandIndex].HandValue
// 	// }

// 	return bestHandIndex
// }

func (gs *GameState) RemoveFoldedPlayerFromEligibleWinners(playerName string) {
	for i := 0; i < len(gs.Pot[0].EligibleWinners); i++ {
		if playerName == gs.Pot[0].EligibleWinners[i] {
			gs.Pot[0].EligibleWinners = remove(gs.Pot[0].EligibleWinners, i)
			// Need to remove the matching string and recreate the eligible winners
		}
	}
}

func remove(slice []string, s int) []string {
	return append(slice[:s], slice[s+1:]...)
}
