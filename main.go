package main

import (
	"poker/config"
	"poker/gamestate"
)

func main() {

	GameConfig := config.NewConfig()
	_ = gamestate.InitializeGame(GameConfig)

}
