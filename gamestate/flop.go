package gamestate

import (
	"fmt"
	"poker/cards"
)

// var flop0 = cards.Card{Value: 3, Suit: "c"}
// var flop1 = cards.Card{Value: 3, Suit: "d"}
// var flop2 = cards.Card{Value: 14, Suit: "s"}

func (gamestate *GameState) DealFlop() {

	for i := 0; i < 3; i++ {
		gamestate.Board = append(gamestate.Board, gamestate.Deck.Deck[0])
		gamestate.Deck.Deck = gamestate.Deck.Deck[1:]
	}
	gamestate.ActionTracker.NbrBets = 0 // Reset the betting
	gamestate.ActionTracker.CurrentTotalBet = 0
	// Reset player betting variables.
	for i := 0; i < len(gamestate.Players); i++ {
		gamestate.Players[i].CheckedorCalled = false
		gamestate.Players[i].Raised = false
		gamestate.Players[i].MoneyInPot = 0
		if !(gamestate.Players[i].Folded) {
			gamestate.Players[i].NoAllowedAction = false
		}
	}

	// gamestate.Board = append(gamestate.Board, flop0)
	// gamestate.Board = append(gamestate.Board, flop1)
	// gamestate.Board = append(gamestate.Board, flop2)

	fmt.Println(fmt.Sprintf("*** FLOP *** [%s %s %s]", cards.ReturnIndividualCardasString(gamestate.Board[0]), cards.ReturnIndividualCardasString(gamestate.Board[1]), cards.ReturnIndividualCardasString(gamestate.Board[2])))

	endFAction := false
	for !endFAction {

		for j := 0; j < len(gamestate.Players); j++ {

			if gamestate.Players[gamestate.ActionTracker.CurrentActorPosition].Folded == false {
				gamestate = PlayerAction(gamestate)

			}
			test, y := checkIfHandOverFromFolding(gamestate.Players)

			if test {
				// log.Println("All other players folded, winner is: ", gamestate.Players[y].Name)
				gamestate.AwardWinnerFromFolds(y)
				endFAction = true
				gamestate.HandIsOver = true
				break
			}
			counter := 0
			for i := 0; i < len(gamestate.Players); i++ {
				if gamestate.Players[gamestate.ActionTracker.CurrentActorPosition].Folded == true || gamestate.Players[gamestate.ActionTracker.CurrentActorPosition].NoAllowedAction == true {
					counter++
				}
			}

			if counter == len(gamestate.Players) {
				endFAction = true
				break
			}

			if j == len(gamestate.Players)-1 {
				j = -1
			}

		}

	}

}
