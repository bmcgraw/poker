package cards

import "testing"

type TestTableStruct struct {
	Input  []AllPossibleHands
	Output []int
}

// func TestFourOfAKindWinner1(T *testing.T) {
// 	var test0card0 = []Card{Card{2, 0}, Card{2, 1}, Card{2, 2}, Card{2, 3}, Card{10, 1}}
// 	var test0card1 = []Card{Card{5, 0}, Card{5, 1}, Card{5, 2}, Card{5, 3}, Card{11, 1}}

// 	FourOfAKindTestStruct := []TestTableStruct{
// 		TestTableStruct{Input: []AllPossibleHands{AllPossibleHands{8, test0card0}, AllPossibleHands{8, test0card1}},
// 			Output: []int{1}},
// 	}

// 	for _, value := range FourOfAKindTestStruct {
// 		testValue := TieBreakerFourOfAKind(value.Input[0], value.Input[1])

// 		if testValue != value.Output[0] {
// 			T.Errorf("Four of a Kind should equal expected output, but four of a kind equals: %d", testValue)
// 		}
// 	}

// }

// func TestFourOfAKindWinner0(T *testing.T) {
// 	var test0card0 = []Card{Card{6, 0}, Card{6, 1}, Card{6, 2}, Card{6, 3}, Card{12, 1}}
// 	var test0card1 = []Card{Card{5, 0}, Card{5, 1}, Card{5, 2}, Card{5, 3}, Card{11, 1}}

// 	FourOfAKindTestStruct := []TestTableStruct{
// 		TestTableStruct{Input: []AllPossibleHands{AllPossibleHands{8, test0card0}, AllPossibleHands{8, test0card1}},
// 			Output: []int{0}},
// 	}

// 	for _, value := range FourOfAKindTestStruct {
// 		testValue := TieBreakerFourOfAKind(value.Input[0], value.Input[1])

// 		if testValue != value.Output[0] {
// 			T.Errorf("Four of a Kind should equal expected output, but four of a kind equals: %d", testValue)
// 		}
// 	}

// }

// func TestFourOfAKindWinnerTie(T *testing.T) {
// 	var test0card0 = []Card{Card{6, 0}, Card{6, 1}, Card{6, 2}, Card{6, 3}, Card{14, 1}}
// 	var test0card1 = []Card{Card{6, 0}, Card{6, 1}, Card{6, 2}, Card{6, 3}, Card{14, 1}}

// 	FourOfAKindTestStruct := []TestTableStruct{
// 		TestTableStruct{Input: []AllPossibleHands{AllPossibleHands{8, test0card0}, AllPossibleHands{8, test0card1}},
// 			Output: []int{-1}},
// 	}

// 	for _, value := range FourOfAKindTestStruct {
// 		testValue := TieBreakerFourOfAKind(value.Input[0], value.Input[1])

// 		if testValue != value.Output[0] {
// 			T.Errorf("Four of a Kind should equal expected output, but four of a kind equals: %d", testValue)
// 		}
// 	}

// }

// func TestFullHouseWinner1(T *testing.T) {
// 	var test0card0 = []Card{Card{2, 0}, Card{2, 1}, Card{2, 2}, Card{3, 3}, Card{3, 1}}
// 	var test0card1 = []Card{Card{4, 0}, Card{4, 1}, Card{4, 2}, Card{2, 3}, Card{2, 1}}

// 	FullHouseTestStruct := []TestTableStruct{
// 		TestTableStruct{Input: []AllPossibleHands{AllPossibleHands{8, test0card0}, AllPossibleHands{8, test0card1}},
// 			Output: []int{1}},
// 	}

// 	for _, value := range FullHouseTestStruct {
// 		testValue := TieBreakerFullHouse(value.Input[0], value.Input[1])

// 		if testValue != value.Output[0] {
// 			T.Errorf("Full House should equal expected output, but full house equals: %d", testValue)
// 		}
// 	}

// }

// func TestFullHouseWinner1pt1(T *testing.T) {
// 	var test0card0 = []Card{Card{4, 0}, Card{4, 1}, Card{4, 2}, Card{3, 3}, Card{3, 1}}
// 	var test0card1 = []Card{Card{4, 0}, Card{4, 1}, Card{4, 2}, Card{5, 3}, Card{5, 1}}

// 	FullHouseTestStruct := []TestTableStruct{
// 		TestTableStruct{Input: []AllPossibleHands{AllPossibleHands{8, test0card0}, AllPossibleHands{8, test0card1}},
// 			Output: []int{1}},
// 	}

// 	for _, value := range FullHouseTestStruct {
// 		testValue := TieBreakerFullHouse(value.Input[0], value.Input[1])

// 		if testValue != value.Output[0] {
// 			T.Errorf("Four of a Kind should equal expected output, but four of a kind equals: %d", testValue)
// 		}
// 	}

// }

// func TestFullHouseWinner0(T *testing.T) {
// 	var test0card0 = []Card{Card{5, 0}, Card{5, 1}, Card{5, 2}, Card{3, 3}, Card{3, 1}}
// 	var test0card1 = []Card{Card{4, 0}, Card{4, 1}, Card{4, 2}, Card{2, 3}, Card{2, 1}}

// 	FullHouseTestStruct := []TestTableStruct{
// 		TestTableStruct{Input: []AllPossibleHands{AllPossibleHands{8, test0card0}, AllPossibleHands{8, test0card1}},
// 			Output: []int{0}},
// 	}

// 	for _, value := range FullHouseTestStruct {
// 		testValue := TieBreakerFullHouse(value.Input[0], value.Input[1])

// 		if testValue != value.Output[0] {
// 			T.Errorf("Four of a Kind should equal expected output, but four of a kind equals: %d", testValue)
// 		}
// 	}

// }

// func TestFullHouseTie(T *testing.T) {
// 	var test0card0 = []Card{Card{5, 0}, Card{5, 1}, Card{5, 2}, Card{3, 3}, Card{3, 1}}
// 	var test0card1 = []Card{Card{5, 0}, Card{5, 1}, Card{5, 2}, Card{3, 3}, Card{3, 1}}

// 	FullHouseTestStruct := []TestTableStruct{
// 		TestTableStruct{Input: []AllPossibleHands{AllPossibleHands{8, test0card0}, AllPossibleHands{8, test0card1}},
// 			Output: []int{-1}},
// 	}

// 	for _, value := range FullHouseTestStruct {
// 		testValue := TieBreakerFullHouse(value.Input[0], value.Input[1])

// 		if testValue != value.Output[0] {
// 			T.Errorf("Four of a Kind should equal expected output, but four of a kind equals: %d", testValue)
// 		}
// 	}

// }

// func TestFlushWinner0(T *testing.T) {
// 	var test0card0 = []Card{Card{5, 0}, Card{6, 0}, Card{7, 0}, Card{8, 0}, Card{11, 0}}
// 	var test0card1 = []Card{Card{5, 0}, Card{6, 0}, Card{7, 0}, Card{8, 0}, Card{10, 0}}

// 	FlushTestStruct := []TestTableStruct{
// 		TestTableStruct{Input: []AllPossibleHands{AllPossibleHands{8, test0card0}, AllPossibleHands{8, test0card1}},
// 			Output: []int{0}},
// 	}

// 	for _, value := range FlushTestStruct {
// 		testValue := TieBreakerFlushOrStraight(value.Input[0], value.Input[1])

// 		if testValue != value.Output[0] {
// 			T.Errorf("Flush should equal expected output, but flush equals: %d", testValue)
// 		}
// 	}

// }

// func TestFlushWinner0pt1(T *testing.T) {
// 	var test0card0 = []Card{Card{5, 0}, Card{6, 0}, Card{7, 0}, Card{8, 0}, Card{10, 0}}
// 	var test0card1 = []Card{Card{2, 0}, Card{6, 0}, Card{7, 0}, Card{8, 0}, Card{10, 0}}

// 	FlushTestStruct := []TestTableStruct{
// 		TestTableStruct{Input: []AllPossibleHands{AllPossibleHands{8, test0card0}, AllPossibleHands{8, test0card1}},
// 			Output: []int{0}},
// 	}

// 	for _, value := range FlushTestStruct {
// 		testValue := TieBreakerFlushOrStraight(value.Input[0], value.Input[1])

// 		if testValue != value.Output[0] {
// 			T.Errorf("Flush should equal expected output, but flush equals: %d", testValue)
// 		}
// 	}

// }

// func TestFlushWinner1pt0(T *testing.T) {
// 	var test0card0 = []Card{Card{5, 0}, Card{6, 0}, Card{7, 0}, Card{8, 0}, Card{10, 0}}
// 	var test0card1 = []Card{Card{2, 0}, Card{6, 0}, Card{7, 0}, Card{8, 0}, Card{11, 0}}

// 	FlushTestStruct := []TestTableStruct{
// 		TestTableStruct{Input: []AllPossibleHands{AllPossibleHands{8, test0card0}, AllPossibleHands{8, test0card1}},
// 			Output: []int{1}},
// 	}

// 	for _, value := range FlushTestStruct {
// 		testValue := TieBreakerFlushOrStraight(value.Input[0], value.Input[1])

// 		if testValue != value.Output[0] {
// 			T.Errorf("Flush should equal expected output, but flush equals: %d", testValue)
// 		}
// 	}

// }

// func TestFlushWinnertie(T *testing.T) {
// 	var test0card0 = []Card{Card{5, 0}, Card{6, 0}, Card{7, 0}, Card{8, 0}, Card{11, 0}}
// 	var test0card1 = []Card{Card{5, 0}, Card{6, 0}, Card{7, 0}, Card{8, 0}, Card{11, 0}}

// 	FlushTestStruct := []TestTableStruct{
// 		TestTableStruct{Input: []AllPossibleHands{AllPossibleHands{8, test0card0}, AllPossibleHands{8, test0card1}},
// 			Output: []int{-1}},
// 	}

// 	for _, value := range FlushTestStruct {
// 		testValue := TieBreakerFlushOrStraight(value.Input[0], value.Input[1])

// 		if testValue != value.Output[0] {
// 			T.Errorf("Flush should equal expected output, but flush equals: %d", testValue)
// 		}
// 	}

// }

// func Test3KindWinner0(T *testing.T) {
// 	var test0card0 = []Card{Card{5, 0}, Card{5, 1}, Card{5, 2}, Card{8, 0}, Card{11, 0}}
// 	var test0card1 = []Card{Card{4, 0}, Card{4, 1}, Card{4, 2}, Card{8, 0}, Card{11, 0}}

// 	ThreeOfAKindTestStruct := []TestTableStruct{
// 		TestTableStruct{Input: []AllPossibleHands{AllPossibleHands{8, test0card0}, AllPossibleHands{8, test0card1}},
// 			Output: []int{0}},
// 	}

// 	for _, value := range ThreeOfAKindTestStruct {
// 		testValue := TieBreakerThreeOfAKind(value.Input[0], value.Input[1])

// 		if testValue != value.Output[0] {
// 			T.Errorf("3 of a kind test should equal expected output, but equals: %d", testValue)
// 		}
// 	}

// }

// func Test3KindWinner0pt0(T *testing.T) {
// 	var test0card0 = []Card{Card{5, 0}, Card{5, 1}, Card{5, 2}, Card{8, 0}, Card{12, 0}}
// 	var test0card1 = []Card{Card{5, 0}, Card{5, 1}, Card{5, 2}, Card{8, 0}, Card{11, 0}}

// 	ThreeOfAKindTestStruct := []TestTableStruct{
// 		TestTableStruct{Input: []AllPossibleHands{AllPossibleHands{8, test0card0}, AllPossibleHands{8, test0card1}},
// 			Output: []int{0}},
// 	}

// 	for _, value := range ThreeOfAKindTestStruct {
// 		testValue := TieBreakerThreeOfAKind(value.Input[0], value.Input[1])

// 		if testValue != value.Output[0] {
// 			T.Errorf("3 of a kind test should equal expected output, but equals: %d", testValue)
// 		}
// 	}

// }

// func Test3KindWinner1pt0(T *testing.T) {
// 	var test0card0 = []Card{Card{5, 0}, Card{5, 1}, Card{5, 2}, Card{8, 0}, Card{12, 0}}
// 	var test0card1 = []Card{Card{7, 0}, Card{7, 1}, Card{7, 2}, Card{8, 0}, Card{11, 0}}

// 	ThreeOfAKindTestStruct := []TestTableStruct{
// 		TestTableStruct{Input: []AllPossibleHands{AllPossibleHands{8, test0card0}, AllPossibleHands{8, test0card1}},
// 			Output: []int{1}},
// 	}

// 	for _, value := range ThreeOfAKindTestStruct {
// 		testValue := TieBreakerThreeOfAKind(value.Input[0], value.Input[1])

// 		if testValue != value.Output[0] {
// 			T.Errorf("3 of a kind test should equal expected output, but equals: %d", testValue)
// 		}
// 	}

// }

// func Test3KindWinner1pt1(T *testing.T) {
// 	var test0card0 = []Card{Card{5, 0}, Card{5, 1}, Card{5, 2}, Card{8, 0}, Card{11, 0}}
// 	var test0card1 = []Card{Card{5, 0}, Card{5, 1}, Card{5, 2}, Card{9, 0}, Card{11, 0}}

// 	ThreeOfAKindTestStruct := []TestTableStruct{
// 		TestTableStruct{Input: []AllPossibleHands{AllPossibleHands{8, test0card0}, AllPossibleHands{8, test0card1}},
// 			Output: []int{1}},
// 	}

// 	for _, value := range ThreeOfAKindTestStruct {
// 		testValue := TieBreakerThreeOfAKind(value.Input[0], value.Input[1])

// 		if testValue != value.Output[0] {
// 			T.Errorf("3 of a kind test should equal expected output, but equals: %d", testValue)
// 		}
// 	}

// }

// func Test3KindWinnerTie0(T *testing.T) {
// 	var test0card0 = []Card{Card{5, 0}, Card{5, 1}, Card{5, 2}, Card{8, 0}, Card{11, 0}}
// 	var test0card1 = []Card{Card{5, 0}, Card{5, 1}, Card{5, 2}, Card{8, 0}, Card{11, 0}}

// 	ThreeOfAKindTestStruct := []TestTableStruct{
// 		TestTableStruct{Input: []AllPossibleHands{AllPossibleHands{8, test0card0}, AllPossibleHands{8, test0card1}},
// 			Output: []int{-1}},
// 	}

// 	for _, value := range ThreeOfAKindTestStruct {
// 		testValue := TieBreakerThreeOfAKind(value.Input[0], value.Input[1])

// 		if testValue != value.Output[0] {
// 			T.Errorf("3 of a kind test should equal expected output, but equals: %d", testValue)
// 		}
// 	}

// }

func Test3KindWinnerTie1(T *testing.T) {
	var test0card0 = []Card{Card{5, "c"}, Card{5, "d"}, Card{5, "h"}, Card{11, "c"}, Card{8, "s"}}
	var test0card1 = []Card{Card{5, "c"}, Card{5, "d"}, Card{5, "h"}, Card{8, "s"}, Card{11, "c"}}

	ThreeOfAKindTestStruct := []TestTableStruct{
		TestTableStruct{Input: []AllPossibleHands{AllPossibleHands{8, test0card0}, AllPossibleHands{8, test0card1}},
			Output: []int{-1}},
	}

	for _, value := range ThreeOfAKindTestStruct {
		testValue := TieBreakerThreeOfAKind(value.Input[0], value.Input[1])

		if testValue != value.Output[0] {
			T.Errorf("3 of a kind test should equal expected output, but equals: %d", testValue)
		}
	}

}

func Test3KindWinnerTie2(T *testing.T) {
	var test0card0 = []Card{Card{5, "d"}, Card{5, "c"}, Card{11, "d"}, Card{8, "s"}, Card{5, "h"}}
	var test0card1 = []Card{Card{5, "c"}, Card{5, "d"}, Card{5, "h"}, Card{8, "s"}, Card{11, "d"}}

	ThreeOfAKindTestStruct := []TestTableStruct{
		TestTableStruct{Input: []AllPossibleHands{AllPossibleHands{8, test0card0}, AllPossibleHands{8, test0card1}},
			Output: []int{-1}},
	}

	for _, value := range ThreeOfAKindTestStruct {
		testValue := TieBreakerThreeOfAKind(value.Input[0], value.Input[1])

		if testValue != value.Output[0] {
			T.Errorf("3 of a kind test should equal expected output, but equals: %d", testValue)
		}
	}

}

// Full flush on board was causing an error
