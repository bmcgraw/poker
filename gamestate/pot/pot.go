package pot

// Pot is a game.
type Pot struct {
	Size            float64
	EligibleWinners []string
}

func (pot Pot) addToPot(amount float64) {
	pot.Size += amount
}
