package config

import "flag"

type GameConfig struct {
	GameType     string
	NbrPlayers   int
	PlayerChips  float64
	PlayerAction string
}

func NewConfig() GameConfig {

	GamePtr := flag.String("gametype", "limitholdem", " a string")
	PlayersPtr := flag.Int("players", 2, "an int")
	PlayerChipsPtr := flag.Float64("playerchips", 1000, "an int")
	PlayerAction := flag.String("action", "userinput", "a string")
	flag.Parse()

	return GameConfig{GameType: *GamePtr, NbrPlayers: *PlayersPtr, PlayerChips: *PlayerChipsPtr, PlayerAction: *PlayerAction}

}
