// Take a list of equal value hands from an individual player, and find the best hand to submit
// Take a list of winning hands where the values are equal, and return the winner or a split pot.

// Royal flush -> Always split
// Straight flush -> high card
// Four of a kind -> Value of the 4 cards, then the kicker
// Full house -> value of the 3 cards
// Flush -> if high card tied, then 2nd highest card, etc.
// Straight -> Value of high card
// Three of a kind -> value of the 3 cards, then the two kickers
// Two pair -> Value of the high pair, value of the low pair, value of kicker
// One pair -> value of the pair, then up to 3 kickers
// High card -> Up to 5 kickers

package cards

import (
	"fmt"
	"log"
	"math/rand"
	"time"
)

type Card struct {
	Value int
	Suit  string
}

type Deck struct {
	Deck []Card
}

type CardString struct {
	Value string
	Suit  string
}

func New() Deck {

	var deck Deck

	// for j := 1; j <= 4; j++ {
	for i := 2; i <= 14; i++ {
		cardC := Card{Value: i, Suit: "c"}
		cardD := Card{Value: i, Suit: "d"}
		cardH := Card{Value: i, Suit: "h"}
		cardS := Card{Value: i, Suit: "s"}
		deck.Deck = append(deck.Deck, cardC)
		deck.Deck = append(deck.Deck, cardD)
		deck.Deck = append(deck.Deck, cardH)
		deck.Deck = append(deck.Deck, cardS)
	}
	// }
	deck.Shuffle()

	return deck
}

func (deck Deck) Shuffle() {
	for i := 1; i < len(deck.Deck); i++ {
		rand.Seed(time.Now().UnixNano())
		r := rand.Intn(i + 1)
		if i != r {
			deck.Deck[r], deck.Deck[i] = deck.Deck[i], deck.Deck[r]
		}
	}
	// log.Println(deck)
	// deck.Deck[0], deck.Deck[14] = deck.Deck[14], deck.Deck[0]
	// deck.Deck[4], deck.Deck[27] = deck.Deck[27], deck.Deck[4]
	// deck.Deck[5], deck.Deck[40] = deck.Deck[40], deck.Deck[5]
	// deck.Deck[7], deck.Deck[39] = deck.Deck[39], deck.Deck[7]
	// deck.Deck[8], deck.Deck[47] = deck.Deck[47], deck.Deck[8]
	// log.Println(deck)
}

func ReturnCardAsString(cards []Card) []string {
	cardString := make([]CardString, 2)
	log.Println(cards[0].Value)
	for i := 0; i < len(cards); i++ {
		if cards[i].Value < 11 {
			cardString[i].Value = fmt.Sprint(int(cards[i].Value))
		} else if cards[i].Value == 11 {
			cardString[i].Value = "J"
		} else if cards[i].Value == 12 {
			cardString[i].Value = "Q"
		} else if cards[i].Value == 13 {
			cardString[i].Value = "K"
		} else if cards[i].Value == 14 {
			cardString[i].Value = "A"
		}
		cardString[i].Suit = cards[i].Suit
		// 	if cards[i].Suit == 1 { // {1: Clubs, 2: Diamonds, 3: Hearts, 4: Spades}
		// 		cardString[i].Suit = "c"
		// 	} else if cards[i].Suit == 2 {
		// 		cardString[i].Suit = "d"
		// 	} else if cards[i].Suit == 3 {
		// 		cardString[i].Suit = "h"
		// 	} else if cards[i].Suit == 4 {
		// 		cardString[i].Suit = "s"
		// 	}
	}

	returnString := []string{fmt.Sprintf("%s%s", cardString[0].Value, cardString[0].Suit), fmt.Sprintf("%s%s", cardString[1].Value, cardString[1].Suit)}
	// return fmt.Sprintf("%s%s%s%s", cardString[0].Value, cardString[0].Suit, cardString[1].Value, cardString[1].Suit)
	return returnString
}

func ReturnIndividualCardasString(card Card) string {
	cardString := make([]CardString, 1)

	if card.Value < 11 {
		cardString[0].Value = fmt.Sprint(int(card.Value))
	} else if card.Value == 11 {
		cardString[0].Value = "J"
	} else if card.Value == 12 {
		cardString[0].Value = "Q"
	} else if card.Value == 13 {
		cardString[0].Value = "K"
	} else if card.Value == 14 {
		cardString[0].Value = "A"
	}
	cardString[0].Suit = card.Suit
	// if card.Suit == 1 { // {1: Clubs, 2: Diamonds, 3: Hearts, 4: Spades}
	// 	cardString[0].Suit = "c"
	// } else if card.Suit == 2 {
	// 	cardString[0].Suit = "d"
	// } else if card.Suit == 3 {
	// 	cardString[0].Suit = "h"
	// } else if card.Suit == 4 {
	// 	cardString[0].Suit = "s"
	// }

	returnString := fmt.Sprintf("%s%s", cardString[0].Value, cardString[0].Suit)
	return returnString

}
