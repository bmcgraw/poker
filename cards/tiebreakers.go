package cards

/*
	Royal Flush - 10, Straight Flush - 9, Four of a Kind - 8, Full House - 7
	Flush - 6, Straight - 5, Three of a Kind - 4, Two Pair - 3, Pair - 2, High Card - 1
*/
func TieBreaker(hand0 AllPossibleHands, hand1 AllPossibleHands) (tiebreaker int) {

	if hand0.HandValue == 10 {
		tiebreaker = -1
		return
	}

	if hand0.HandValue == 9 {
		for i := 0; i < len(hand0.IndividualHand); i++ {
			if hand0.IndividualHand[i].Value < hand1.IndividualHand[i].Value {
				tiebreaker = 1
				return
			} else if hand0.IndividualHand[i].Value > hand1.IndividualHand[i].Value {
				tiebreaker = 0
				return
			}
			if i == len(hand0.IndividualHand)-1 {
				tiebreaker = -1
				return
			}
		}
	}

	if hand0.HandValue == 8 {
		tiebreaker = TieBreakerFourOfAKind(hand0, hand1)
		return
	}

	if hand0.HandValue == 7 {
		tiebreaker = TieBreakerFullHouse(hand0, hand1)
		return
	}

	if hand0.HandValue == 6 || hand0.HandValue == 5 {
		tiebreaker = TieBreakerFlushOrStraight(hand0, hand1)
		return
	}

	if hand0.HandValue == 4 {
		tiebreaker = TieBreakerThreeOfAKind(hand0, hand1)
		return
	}

	if hand0.HandValue == 3 {
		tiebreaker = TieBreakerTwoPair(hand0, hand1)
		return
	}

	if hand0.HandValue == 2 {
		tiebreaker = TieBreakerPair(hand0, hand1)
		return
	}

	if hand0.HandValue == 1 {
		tiebreaker = TieBreakerHighCard(hand0, hand1)
		return
	}

	return tiebreaker
}

func TieBreakerFourOfAKind(hand0 AllPossibleHands, hand1 AllPossibleHands) int {
	tmp0 := hand0.IndividualHand.returnHandValuesForPairChecking()
	tmp1 := hand1.IndividualHand.returnHandValuesForPairChecking()
	value40 := 0
	value41 := 0
	valuekicker0 := 0
	valuekicker1 := 0

	duplicate_frequency0 := make(map[int]int)
	duplicate_frequency1 := make(map[int]int)

	for _, item0 := range tmp0 {
		// check if the item/element exist in the duplicate_frequency map

		_, exist := duplicate_frequency0[item0]

		if exist {
			duplicate_frequency0[item0] += 1 // increase counter by 1 if already in the map
		} else {
			duplicate_frequency0[item0] = 1 // else start counting from 1
		}

	}

	for _, item1 := range tmp1 {
		// check if the item/element exist in the duplicate_frequency map

		_, exist := duplicate_frequency1[item1]

		if exist {
			duplicate_frequency1[item1] += 1 // increase counter by 1 if already in the map
		} else {
			duplicate_frequency1[item1] = 1 // else start counting from 1
		}
	}

	for i, j := range duplicate_frequency0 {
		if j == 4 {
			value40 = int(i)
		}

		if j == 1 {
			valuekicker0 = int(i)
		}
	}

	for l, m := range duplicate_frequency1 {
		if m == 4 {
			value41 = int(l)
		}

		if m == 1 {
			valuekicker1 = int(l)
		}
	}

	if value40 > value41 {
		return 0
	} else if value40 < value41 {
		return 1
	} else if value40 == value41 {
		if valuekicker0 > valuekicker1 {
			return 0
		} else if valuekicker0 < valuekicker1 {
			return 1
		} else {
			return -1 // True tie
		}
	}

	return -1

}

func TieBreakerFullHouse(hand0 AllPossibleHands, hand1 AllPossibleHands) int {
	tmp0 := hand0.IndividualHand.returnHandValuesForPairChecking()
	tmp1 := hand1.IndividualHand.returnHandValuesForPairChecking()
	valueFH30 := 0
	valueFH31 := 0
	valueFH20 := 0
	valueFH21 := 0

	duplicate_frequency0 := make(map[int]int)
	duplicate_frequency1 := make(map[int]int)

	for _, item0 := range tmp0 {
		// check if the item/element exist in the duplicate_frequency map

		_, exist := duplicate_frequency0[item0]

		if exist {
			duplicate_frequency0[item0] += 1 // increase counter by 1 if already in the map
		} else {
			duplicate_frequency0[item0] = 1 // else start counting from 1
		}

	}

	for _, item1 := range tmp1 {
		// check if the item/element exist in the duplicate_frequency map

		_, exist := duplicate_frequency1[item1]

		if exist {
			duplicate_frequency1[item1] += 1 // increase counter by 1 if already in the map
		} else {
			duplicate_frequency1[item1] = 1 // else start counting from 1
		}
	}

	for i, j := range duplicate_frequency0 {
		if j == 3 {
			valueFH30 = int(i)
		}

		if j == 2 {
			valueFH20 = int(i)
		}
	}

	for l, m := range duplicate_frequency1 {
		if m == 3 {
			valueFH31 = int(l)
		}

		if m == 2 {
			valueFH21 = int(l)
		}
	}

	if valueFH30 > valueFH31 {
		return 0
	} else if valueFH30 < valueFH31 {
		return 1
	} else if valueFH30 == valueFH31 {
		if valueFH20 > valueFH21 {
			return 0
		} else if valueFH20 < valueFH21 {
			return 1
		} else {
			return -1 // True tie
		}
	}

	return -1

}

func TieBreakerFlushOrStraight(hand0 AllPossibleHands, hand1 AllPossibleHands) int {
	tmp0 := hand0.IndividualHand.Sort()
	tmp1 := hand1.IndividualHand.Sort()

	if tmp0[4].Value > tmp1[4].Value {
		return 0
	} else if tmp0[4].Value < tmp1[4].Value {
		return 1
	}

	if tmp0[3].Value > tmp1[3].Value {
		return 0
	} else if tmp0[3].Value < tmp1[3].Value {
		return 1
	}

	if tmp0[2].Value > tmp1[2].Value {
		return 0
	} else if tmp0[2].Value < tmp1[2].Value {
		return 1
	}

	if tmp0[1].Value > tmp1[1].Value {
		return 0
	} else if tmp0[1].Value < tmp1[1].Value {
		return 1
	}

	if tmp0[0].Value > tmp1[0].Value {
		return 0
	} else if tmp0[0].Value < tmp1[0].Value {
		return 1
	}

	return -1

}

func TieBreakerThreeOfAKind(hand0 AllPossibleHands, hand1 AllPossibleHands) int {
	hand0.IndividualHand.Sort()
	tmp0 := hand0.IndividualHand.returnHandValuesForPairChecking()
	hand1.IndividualHand.Sort()
	tmp1 := hand1.IndividualHand.returnHandValuesForPairChecking()

	// log.Println(tmp0)
	// log.Println(tmp1)

	value30 := 0
	value31 := 0
	valuekicker00 := 0
	valuekicker01 := 0
	valuekicker10 := 0
	valuekicker11 := 0

	duplicate_frequency0 := make(map[int]int)
	duplicate_frequency1 := make(map[int]int)

	for _, item0 := range tmp0 {
		// check if the item/element exist in the duplicate_frequency map

		_, exist := duplicate_frequency0[item0]

		if exist {
			duplicate_frequency0[item0] += 1 // increase counter by 1 if already in the map
		} else {
			duplicate_frequency0[item0] = 1 // else start counting from 1
		}

	}

	for _, item1 := range tmp1 {
		// check if the item/element exist in the duplicate_frequency map

		_, exist1 := duplicate_frequency1[item1]

		if exist1 {
			duplicate_frequency1[item1] += 1 // increase counter by 1 if already in the map
		} else {
			duplicate_frequency1[item1] = 1 // else start counting from 1
		}
	}

	for i, j := range duplicate_frequency0 {
		if j == 3 {
			value30 = int(i)
		}

		if j == 1 && valuekicker00 == 0 {
			valuekicker00 = int(i)
		}
		if j == 1 && valuekicker00 != 0 {
			valuekicker01 = int(i)
		}
	}

	for l, m := range duplicate_frequency1 {
		if m == 3 {
			value31 = int(l)
		}

		if m == 1 && valuekicker10 == 0 {
			valuekicker10 = int(l)
		}
		if m == 1 && valuekicker10 != 0 {
			valuekicker11 = int(l)
		}
	}

	// fmt.Println(value30, valuekicker01, valuekicker00)
	// fmt.Println(value31, valuekicker11, valuekicker10)

	if value30 > value31 {
		return 0
	} else if value30 < value31 {
		return 1
	} else if value30 == value31 {
		// log.Println("in this if  block")
		if valuekicker01 > valuekicker11 {
			return 0
		} else if valuekicker01 < valuekicker11 {
			return 1
		} else {
			// log.Println("In second if block", valuekicker00, "is valuekicker00, and valuekicker10 is: ", valuekicker10)
			if valuekicker00 > valuekicker10 {
				return 0
			} else if valuekicker00 < valuekicker10 {
				return 1
			} else {
				// log.Println("Should be returning -1")
				return -1 // True tie
			}
		}
	}
	return -1
}

func TieBreakerTwoPair(hand0 AllPossibleHands, hand1 AllPossibleHands) int {
	hand0.IndividualHand.Sort()
	tmp0 := hand0.IndividualHand.returnHandValuesForPairChecking()
	hand1.IndividualHand.Sort()
	tmp1 := hand1.IndividualHand.returnHandValuesForPairChecking()

	duplicate_frequency0 := make(map[int]int)
	duplicate_frequency1 := make(map[int]int)

	for _, item0 := range tmp0 {
		// check if the item/element exist in the duplicate_frequency map

		_, exist := duplicate_frequency0[item0]

		if exist {
			duplicate_frequency0[item0] += 1 // increase counter by 1 if already in the map
		} else {
			duplicate_frequency0[item0] = 1 // else start counting from 1
		}

	}

	for _, item1 := range tmp1 {
		// check if the item/element exist in the duplicate_frequency map

		_, exist1 := duplicate_frequency1[item1]

		if exist1 {
			duplicate_frequency1[item1] += 1 // increase counter by 1 if already in the map
		} else {
			duplicate_frequency1[item1] = 1 // else start counting from 1
		}
	}

	highpair0 := 0
	lowpair0 := 0
	kicker0 := 0
	for i, j := range duplicate_frequency0 {
		if j == 2 && lowpair0 == 0 {
			lowpair0 = int(i)
		} else if j == 2 && lowpair0 != 0 {
			highpair0 = int(i)
		} else if j == 1 {
			kicker0 = int(i)
		}

		if highpair0 < lowpair0 {
			highpair0, lowpair0 = lowpair0, highpair0
		}
	}

	highpair1 := 0
	lowpair1 := 0
	kicker1 := 0
	for l, m := range duplicate_frequency1 {
		if m == 2 && lowpair1 == 1 {
			lowpair1 = int(l)
		} else if m == 2 && lowpair1 != 1 {
			highpair1 = int(l)
		} else if m == 1 {
			kicker1 = int(l)
		}

		if highpair1 < lowpair1 {
			highpair1, lowpair1 = lowpair1, highpair1
		}
	}

	if highpair0 > highpair1 {
		return 0
	} else if highpair0 < highpair1 {
		return 1
	}

	if lowpair0 > lowpair1 {
		return 0
	} else if lowpair0 < lowpair1 {
		return 1
	}

	if kicker0 > kicker1 {
		return 0
	} else if kicker0 < kicker1 {
		return 1
	}

	return -1
}

func TieBreakerPair(hand0 AllPossibleHands, hand1 AllPossibleHands) int {
	hand0.IndividualHand.Sort()
	tmp0 := hand0.IndividualHand.returnHandValuesForPairChecking()
	hand1.IndividualHand.Sort()
	tmp1 := hand1.IndividualHand.returnHandValuesForPairChecking()

	duplicate_frequency0 := make(map[int]int)
	duplicate_frequency1 := make(map[int]int)

	for _, item0 := range tmp0 {
		// check if the item/element exist in the duplicate_frequency map

		_, exist := duplicate_frequency0[item0]

		if exist {
			duplicate_frequency0[item0] += 1 // increase counter by 1 if already in the map
		} else {
			duplicate_frequency0[item0] = 1 // else start counting from 1
		}

	}

	for _, item1 := range tmp1 {
		// check if the item/element exist in the duplicate_frequency map

		_, exist1 := duplicate_frequency1[item1]

		if exist1 {
			duplicate_frequency1[item1] += 1 // increase counter by 1 if already in the map
		} else {
			duplicate_frequency1[item1] = 1 // else start counting from 1
		}
	}

	pair0 := 0
	for i, j := range duplicate_frequency0 {
		if j == 2 {
			pair0 = int(i)
		}
	}

	pair1 := 0
	for l, m := range duplicate_frequency1 {
		if m == 2 {
			pair1 = int(l)
		}
	}

	for i := 0; i < len(hand0.IndividualHand); i++ {
		if hand0.IndividualHand[i].Value == pair0 {
			hand0.IndividualHand = removeCardFromHandForTB(hand0.IndividualHand, i)
		}
	}

	for i := 0; i < len(hand1.IndividualHand); i++ {
		if hand1.IndividualHand[i].Value == pair1 {
			hand1.IndividualHand = removeCardFromHandForTB(hand1.IndividualHand, i)
		}
	}

	if pair0 > pair1 {
		return 0
	} else if pair0 < pair1 {
		return 1
	}

	if hand0.IndividualHand[2].Value > hand1.IndividualHand[2].Value {
		return 0
	} else if hand0.IndividualHand[2].Value < hand1.IndividualHand[2].Value {
		return 1
	}

	if hand0.IndividualHand[1].Value > hand1.IndividualHand[1].Value {
		return 0
	} else if hand0.IndividualHand[1].Value < hand1.IndividualHand[1].Value {
		return 1
	}

	if hand0.IndividualHand[0].Value > hand1.IndividualHand[0].Value {
		return 0
	} else if hand0.IndividualHand[0].Value < hand1.IndividualHand[0].Value {
		return 1
	}

	return -1
}

func TieBreakerHighCard(hand0 AllPossibleHands, hand1 AllPossibleHands) int {
	hand0.IndividualHand.Sort()
	hand1.IndividualHand.Sort()

	if hand0.IndividualHand[4].Value > hand1.IndividualHand[4].Value {
		return 0
	} else if hand0.IndividualHand[4].Value < hand1.IndividualHand[4].Value {
		return 1
	}

	if hand0.IndividualHand[3].Value > hand1.IndividualHand[3].Value {
		return 0
	} else if hand0.IndividualHand[3].Value < hand1.IndividualHand[3].Value {
		return 1
	}

	if hand0.IndividualHand[2].Value > hand1.IndividualHand[2].Value {
		return 0
	} else if hand0.IndividualHand[2].Value < hand1.IndividualHand[2].Value {
		return 1
	}

	if hand0.IndividualHand[1].Value > hand1.IndividualHand[1].Value {
		return 0
	} else if hand0.IndividualHand[1].Value < hand1.IndividualHand[1].Value {
		return 1
	}

	if hand0.IndividualHand[0].Value > hand1.IndividualHand[0].Value {
		return 0
	} else if hand0.IndividualHand[0].Value < hand1.IndividualHand[0].Value {
		return 1
	}

	return -1

}

func removeCardFromHandForTB(slice Hand, s int) Hand {
	return append(slice[:s], slice[s+1:]...)
}
