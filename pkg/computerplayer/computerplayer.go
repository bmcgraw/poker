package computerplayer

import (
	"math/rand"
	"time"
)

func ComputerActions(o []string) string {
	// Raise, Check, Call, Fold

	if Equal(o, []string{"Call", "Fold"}) {
		o = []string{"Fold", "Call", "Call", "Call", "Call", "Call", "Call", "Call", "Call", "Call"}
	}
	if Equal(o, []string{"Raise", "Check"}) {
		o = []string{"Check", "Check", "Check", "Check", "Check", "Raise", "Raise", "Raise", "Raise", "Raise"}
	}
	if Equal(o, []string{"Raise", "Call", "Fold"}) {
		o = []string{"Fold", "Call", "Call", "Call", "Call", "Call", "Raise", "Raise", "Raise", "Raise"}
	}

	rand.Seed(time.Now().Unix())
	return o[rand.Intn(len(o))]
}

func Equal(a, b []string) bool {
	if len(a) != len(b) {
		return false
	}
	for i, v := range a {
		if v != b[i] {
			return false
		}
	}
	return true
}
