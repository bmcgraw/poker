package gamestate

import (
	"fmt"
	"poker/cards"
)

// var turn0 = cards.Card{Value: 10, Suit: "d"}

func (gamestate *GameState) DealTurn() {

	gamestate.ActionTracker.NbrBets = 0 // Reset the betting
	gamestate.ActionTracker.Multiplier = 2
	gamestate.ActionTracker.CurrentTotalBet = 0

	// Reset player betting variables.
	for i := 0; i < len(gamestate.Players); i++ {
		gamestate.Players[i].CheckedorCalled = false
		gamestate.Players[i].Raised = false
		gamestate.Players[i].MoneyInPot = 0
		if !(gamestate.Players[i].Folded) {
			gamestate.Players[i].NoAllowedAction = false
		}
	}

	gamestate.Board = append(gamestate.Board, gamestate.Deck.Deck[0])
	// gamestate.Board = append(gamestate.Board, turn0)
	gamestate.Deck.Deck = gamestate.Deck.Deck[1:]

	fmt.Println(fmt.Sprintf("*** TURN *** [%s %s %s] [%s]", cards.ReturnIndividualCardasString(gamestate.Board[0]), cards.ReturnIndividualCardasString(gamestate.Board[1]), cards.ReturnIndividualCardasString(gamestate.Board[2]), cards.ReturnIndividualCardasString(gamestate.Board[3])))

	endTurnAction := false
	for !endTurnAction {

		for j := 0; j < len(gamestate.Players); j++ {

			if gamestate.Players[gamestate.ActionTracker.CurrentActorPosition].Folded == false {
				gamestate = PlayerAction(gamestate)

			}
			test, y := checkIfHandOverFromFolding(gamestate.Players)

			if test {
				gamestate.AwardWinnerFromFolds(y)
				endTurnAction = true
				gamestate.HandIsOver = true
				break
			}
			counter := 0
			for i := 0; i < len(gamestate.Players); i++ {
				if gamestate.Players[gamestate.ActionTracker.CurrentActorPosition].Folded == true || gamestate.Players[gamestate.ActionTracker.CurrentActorPosition].NoAllowedAction == true {
					counter++
				}
			}

			if counter == len(gamestate.Players) {
				endTurnAction = true
				break
			}

			if j == len(gamestate.Players)-1 {
				j = -1
			}

		}

	}
}
