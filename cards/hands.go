package cards

type Hand []Card

type AllPossibleHands struct {
	HandValue      int
	IndividualHand Hand
}

func (hand Hand) Sort() Hand {

	for i := 0; i < len(hand)-1; i++ {
		for j := 0; j < len(hand)-1; j++ {
			if hand[j].Value > hand[j+1].Value {
				tmpV := hand[j].Value
				tmpS := hand[j].Suit

				hand[j].Value = hand[j+1].Value
				hand[j].Suit = hand[j+1].Suit

				hand[j+1].Value = tmpV
				hand[j+1].Suit = tmpS
			}
		}

	}

	return hand

}

func (hand Hand) Value() int {
	/*
		Royal Flush - 10, Straight Flush - 9, Four of a Kind - 8, Full House - 7
		Flush - 6, Straight - 5, Three of a Kind - 4, Two Pair - 3, Pair - 2, High Card - 1
	*/
	hand.Sort()

	handValue := hand.checkForStraightOrFlush()
	if handValue > 0 {
		return handValue
	}

	handValue = hand.checkForPairs()

	return handValue

}

func (hand Hand) checkForPairs() int {
	tmp := hand.returnHandValuesForPairChecking()
	var new []int

	duplicate_frequency := make(map[int]int)

	for _, item := range tmp {
		// check if the item/element exist in the duplicate_frequency map

		_, exist := duplicate_frequency[item]

		if exist {
			duplicate_frequency[item] += 1 // increase counter by 1 if already in the map
		} else {
			duplicate_frequency[item] = 1 // else start counting from 1
		}
	}

	for _, Value := range duplicate_frequency {
		new = append(new, Value)

	}
	if len(new) == 5 {
		return 1 // If the length is five, there are no pairs
	} else if len(new) == 4 {
		return 2 // If the length is four, there is one pair
	} else if len(new) == 3 && (new[0] == 3 || new[1] == 3 || new[2] == 3) {
		return 4
		// If the total length is there, and one of the individual arrays has length 3, its 3 of a kind
	} else if len(new) == 3 {
		return 3 // Otherwise, it is two pair
	} else if len(new) == 2 && (new[0] == 4 || new[1] == 4) {
		return 8 // Four of a kind
	} else {
		return 7 // If nothing else, full house
	}

}

func (hand Hand) checkForStraightOrFlush() int {

	/* Is Hand a Straight */
	straight := true
	for i := 0; i < len(hand)-1; i++ {
		if hand[i].Value != (hand[i+1].Value - 1) {
			straight = false
			break
		}

	}

	/* Is Hand a Flush */
	flush := true
	for j := 0; j < len(hand)-1; j++ {
		if hand[j].Suit != hand[j+1].Suit {
			flush = false
			break
		}
	}
	// Is Hand a straight or royal flush
	if straight && flush {
		// var tmp []int
		// royalFlush := []int{10, 11, 12, 13, 14}
		if hand[0].Value == 10 && hand[4].Value == 14 {
			return 10
		} else {
			return 9
		}
	} else if straight && !flush {
		return 5
	} else if !straight && flush {
		return 6
	}
	return 0
}

func (hand Hand) returnHandValuesForPairChecking() []int {
	var tmp []int
	for i := 0; i < len(hand); i++ {
		tmp = append(tmp, hand[i].Value)
	}
	return tmp
}

func GenerateAllPossible5HandCombos(hand Hand) []AllPossibleHands {
	var allPossibleHands []AllPossibleHands

	allPossibleHands = append(allPossibleHands, AllPossibleHands{HandValue: 0, IndividualHand: Hand{hand[2], hand[3], hand[4], hand[5], hand[6]}})
	allPossibleHands = append(allPossibleHands, AllPossibleHands{HandValue: 0, IndividualHand: Hand{hand[1], hand[3], hand[4], hand[5], hand[6]}})
	allPossibleHands = append(allPossibleHands, AllPossibleHands{HandValue: 0, IndividualHand: Hand{hand[1], hand[2], hand[4], hand[5], hand[6]}})
	allPossibleHands = append(allPossibleHands, AllPossibleHands{HandValue: 0, IndividualHand: Hand{hand[1], hand[2], hand[3], hand[5], hand[6]}})
	allPossibleHands = append(allPossibleHands, AllPossibleHands{HandValue: 0, IndividualHand: Hand{hand[1], hand[2], hand[3], hand[4], hand[6]}})
	allPossibleHands = append(allPossibleHands, AllPossibleHands{HandValue: 0, IndividualHand: Hand{hand[1], hand[2], hand[3], hand[4], hand[5]}})

	allPossibleHands = append(allPossibleHands, AllPossibleHands{HandValue: 0, IndividualHand: Hand{hand[0], hand[3], hand[4], hand[5], hand[6]}})
	allPossibleHands = append(allPossibleHands, AllPossibleHands{HandValue: 0, IndividualHand: Hand{hand[0], hand[2], hand[4], hand[5], hand[6]}})
	allPossibleHands = append(allPossibleHands, AllPossibleHands{HandValue: 0, IndividualHand: Hand{hand[0], hand[2], hand[3], hand[5], hand[6]}})
	allPossibleHands = append(allPossibleHands, AllPossibleHands{HandValue: 0, IndividualHand: Hand{hand[0], hand[2], hand[3], hand[4], hand[6]}})
	allPossibleHands = append(allPossibleHands, AllPossibleHands{HandValue: 0, IndividualHand: Hand{hand[0], hand[2], hand[3], hand[4], hand[5]}})

	allPossibleHands = append(allPossibleHands, AllPossibleHands{HandValue: 0, IndividualHand: Hand{hand[0], hand[1], hand[4], hand[5], hand[6]}})
	allPossibleHands = append(allPossibleHands, AllPossibleHands{HandValue: 0, IndividualHand: Hand{hand[0], hand[1], hand[3], hand[5], hand[6]}})
	allPossibleHands = append(allPossibleHands, AllPossibleHands{HandValue: 0, IndividualHand: Hand{hand[0], hand[1], hand[3], hand[4], hand[6]}})
	allPossibleHands = append(allPossibleHands, AllPossibleHands{HandValue: 0, IndividualHand: Hand{hand[0], hand[1], hand[3], hand[4], hand[5]}})

	allPossibleHands = append(allPossibleHands, AllPossibleHands{HandValue: 0, IndividualHand: Hand{hand[0], hand[1], hand[2], hand[5], hand[6]}})
	allPossibleHands = append(allPossibleHands, AllPossibleHands{HandValue: 0, IndividualHand: Hand{hand[0], hand[1], hand[2], hand[4], hand[6]}})
	allPossibleHands = append(allPossibleHands, AllPossibleHands{HandValue: 0, IndividualHand: Hand{hand[0], hand[1], hand[2], hand[4], hand[5]}})

	allPossibleHands = append(allPossibleHands, AllPossibleHands{HandValue: 0, IndividualHand: Hand{hand[0], hand[1], hand[2], hand[3], hand[6]}})
	allPossibleHands = append(allPossibleHands, AllPossibleHands{HandValue: 0, IndividualHand: Hand{hand[0], hand[1], hand[2], hand[3], hand[5]}})

	allPossibleHands = append(allPossibleHands, AllPossibleHands{HandValue: 0, IndividualHand: Hand{hand[0], hand[1], hand[2], hand[3], hand[4]}})

	return allPossibleHands
}

func HandValueAsString(n int) string {
	/*
		Royal Flush - 10, Straight Flush - 9, Four of a Kind - 8, Full House - 7
		Flush - 6, Straight - 5, Three of a Kind - 4, Two Pair - 3, Pair - 2, High Card - 1
	*/

	switch n {
	case 10:
		return "Royal Flush"

	case 9:
		return "Straight Flush"

	case 8:
		return "Four of a Kind"

	case 7:
		return "Full House"

	case 6:
		return "Flush"

	case 5:
		return "Straight"

	case 4:
		return "Three of a Kind"

	case 3:
		return "Two Pair"

	case 2:
		return "One Pair"

	case 1:
		return "High Card"

	default:
		return "Error"
	}
}
