package gamestate

import (
	"log"
	"poker/cards"
	"poker/config"
	"poker/gamestate/player"
	"poker/gamestate/pot"
	"poker/pkg/computerplayer"
	"poker/pkg/util/humanplayerinput"
)

type GameState struct {
	Round          int
	Players        []player.Player
	CurrentActor   int
	Pot            []pot.Pot
	Deck           cards.Deck
	BigBlindSize   int
	ActionTracker  Action
	DealerPosition int
	HandIsOver     bool
	Board          []cards.Card
}

// InitializeGame Initializes the game.
func InitializeGame(c config.GameConfig) GameState {
	players := make([]player.Player, c.NbrPlayers)
	var playerNames []string
	for i := 0; i < len(players); i++ {
		// players[i].Name = fmt.Sprintf("Player %d", i)
		players[i].Chips = c.PlayerChips

		if i == 1 {
			players[i].TypeOfPlayer = "human"
			players[i].Name = "Brian"
		} else {
			players[i].TypeOfPlayer = "computer"
			players[i].Name = "Computer"
		}
		playerNames = append(playerNames, players[i].Name)
	}

	pot := []pot.Pot{pot.Pot{Size: 0, EligibleWinners: playerNames}}
	deck := cards.New()

	GameState := GameState{Round: 0, Players: players, Pot: pot, Deck: deck, BigBlindSize: 20}
	GameState = PlayGame(GameState)
	if GameState.HandIsOver == true {

		return GameState
	}

	return GameState
}

// PlayGame plays the game
func PlayGame(gs GameState) GameState {
	// // Assign the dealer position
	// // Figure out how to wrap the arrays w.r.t. whose turn it is

	gs.PreFlop()

	if !gs.HandIsOver {
		gs.DealFlop()

	}

	if !gs.HandIsOver {
		gs.DealTurn()
	}

	if !gs.HandIsOver {
		gs.DealRiver()
	}

	if !gs.HandIsOver {
		gs.AwardWinner()
	}

	log.Println(gs.Players[0].Name, "has", gs.Players[0].Chips, "chips.")
	log.Println(gs.Players[1].Name, "has", gs.Players[1].Chips, "chips.")

	return gs

}

// PlayerAction is player action.
func PlayerAction(gs *GameState) *GameState {
	var options []string
	if gs.ActionTracker.NbrBets == 4 && (gs.Players[gs.ActionTracker.CurrentActorPosition].MoneyInPot < gs.ActionTracker.CurrentTotalBet) {
		options = []string{"Call", "Fold"}
	} else if gs.ActionTracker.NbrBets == 4 && (gs.Players[gs.ActionTracker.CurrentActorPosition].MoneyInPot == gs.ActionTracker.CurrentTotalBet) {
		gs.Players[gs.ActionTracker.CurrentActorPosition].NoAllowedAction = true
		return gs
	} else if gs.ActionTracker.NbrBets < 4 && (gs.Players[gs.ActionTracker.CurrentActorPosition].MoneyInPot == gs.ActionTracker.CurrentTotalBet) && gs.Players[gs.ActionTracker.CurrentActorPosition].Raised == true {
		gs.Players[gs.ActionTracker.CurrentActorPosition].NoAllowedAction = true
		return gs
	} else if gs.ActionTracker.NbrBets < 4 && (gs.Players[gs.ActionTracker.CurrentActorPosition].MoneyInPot == gs.ActionTracker.CurrentTotalBet) && gs.Players[gs.ActionTracker.CurrentActorPosition].Raised == false && gs.Players[gs.ActionTracker.CurrentActorPosition].CheckedorCalled == true {
		gs.Players[gs.ActionTracker.CurrentActorPosition].NoAllowedAction = true
		return gs
	} else if (gs.ActionTracker.NbrBets == 0) && (gs.Players[gs.ActionTracker.CurrentActorPosition].MoneyInPot == gs.ActionTracker.CurrentTotalBet) && gs.Players[gs.ActionTracker.CurrentActorPosition].Raised == false {
		options = []string{"Bet", "Check"}
	} else if (gs.ActionTracker.NbrBets < 4 && gs.ActionTracker.NbrBets > 0) && (gs.Players[gs.ActionTracker.CurrentActorPosition].MoneyInPot == gs.ActionTracker.CurrentTotalBet) && gs.Players[gs.ActionTracker.CurrentActorPosition].Raised == false {
		options = []string{"Raise", "Check"}
	} else if gs.ActionTracker.NbrBets < 4 && (gs.Players[gs.ActionTracker.CurrentActorPosition].MoneyInPot < gs.ActionTracker.CurrentTotalBet) {
		options = []string{"Raise", "Call", "Fold"}
	} else {
		// log.Println("nbr bets", gs.ActionTracker.NbrBets)
		// log.Println("players", gs.Players)
		options = []string{"Unknown"}
	}

	var option string

	if (gs.Players[gs.ActionTracker.CurrentActorPosition].TypeOfPlayer) == "computer" {
		option = computerplayer.ComputerActions(options)
	} else if (gs.Players[gs.ActionTracker.CurrentActorPosition].TypeOfPlayer) == "human" {
		option = humanplayerinput.ReadHumanPlayerInput(gs.Players[gs.ActionTracker.CurrentActorPosition].Name, options)
	}

	// log.Printf("%s (%s) %ss %f.", gs.Players[gs.ActionTracker.CurrentActorPosition].Name, gs.Players[gs.ActionTracker.CurrentActorPosition].TypeOfPlayer, option, gs.ActionTracker.CurrentTotalBet)

	if option == "Fold" {
		log.Printf("%s (%s) folds.", gs.Players[gs.ActionTracker.CurrentActorPosition].Name, gs.Players[gs.ActionTracker.CurrentActorPosition].TypeOfPlayer)
		gs.Players[gs.ActionTracker.CurrentActorPosition].Folded = true
		gs.Players[gs.ActionTracker.CurrentActorPosition].NoAllowedAction = true
		gs.ActionTracker.CurrentActorPosition = ifMaxPlayer(gs.ActionTracker.CurrentActorPosition, len(gs.Players)) // if there are only two players, and one folds, end the hand
		gs.RemoveFoldedPlayerFromEligibleWinners(gs.Players[gs.ActionTracker.CurrentActorPosition].Name)
		return gs
	} else if option == "Check" {
		log.Printf("%s (%s) checks.", gs.Players[gs.ActionTracker.CurrentActorPosition].Name, gs.Players[gs.ActionTracker.CurrentActorPosition].TypeOfPlayer)
		gs.Players[gs.ActionTracker.CurrentActorPosition].CheckedorCalled = true
		gs.ActionTracker.CurrentActorPosition = ifMaxPlayer(gs.ActionTracker.CurrentActorPosition, len(gs.Players))
	} else if option == "Raise" || option == "Bet" {
		gs.ActionTracker.NbrBets++
		changeInTotalBetValue := float64(gs.BigBlindSize * gs.ActionTracker.Multiplier)
		gs.ActionTracker.CurrentTotalBet += changeInTotalBetValue
		gs.Pot[0].Size += (gs.ActionTracker.CurrentTotalBet - gs.Players[gs.ActionTracker.CurrentActorPosition].MoneyInPot)
		gs.Players[gs.ActionTracker.CurrentActorPosition].Chips = gs.Players[gs.ActionTracker.CurrentActorPosition].Chips - (gs.ActionTracker.CurrentTotalBet - gs.Players[gs.ActionTracker.CurrentActorPosition].MoneyInPot)
		gs.Players[gs.ActionTracker.CurrentActorPosition].MoneyInPot = gs.Players[gs.ActionTracker.CurrentActorPosition].MoneyInPot + (gs.ActionTracker.CurrentTotalBet - gs.Players[gs.ActionTracker.CurrentActorPosition].MoneyInPot)
		gs.Players[gs.ActionTracker.CurrentActorPosition].MoneyInPot = gs.ActionTracker.CurrentTotalBet
		gs.Players[gs.ActionTracker.CurrentActorPosition].Raised = true
		gs.ActionTracker.CurrentActorPosition = ifMaxPlayer(gs.ActionTracker.CurrentActorPosition, len(gs.Players))
		if option == "Raise" {
			log.Printf("%s (%s) raises %.0f to %.0f.", gs.Players[gs.ActionTracker.CurrentActorPosition].Name, gs.Players[gs.ActionTracker.CurrentActorPosition].TypeOfPlayer, changeInTotalBetValue, gs.ActionTracker.CurrentTotalBet)
		} else {
			log.Printf("%s (%s) bets %.0f.", gs.Players[gs.ActionTracker.CurrentActorPosition].Name, gs.Players[gs.ActionTracker.CurrentActorPosition].TypeOfPlayer, changeInTotalBetValue)
		}

	} else if option == "Call" {
		gs.Pot[0].Size += (gs.ActionTracker.CurrentTotalBet - gs.Players[gs.ActionTracker.CurrentActorPosition].MoneyInPot)
		gs.Players[gs.ActionTracker.CurrentActorPosition].Chips -= (gs.ActionTracker.CurrentTotalBet - gs.Players[gs.ActionTracker.CurrentActorPosition].MoneyInPot)
		gs.Players[gs.ActionTracker.CurrentActorPosition].MoneyInPot = gs.ActionTracker.CurrentTotalBet
		gs.Players[gs.ActionTracker.CurrentActorPosition].Raised = false
		gs.Players[gs.ActionTracker.CurrentActorPosition].CheckedorCalled = true
		gs.ActionTracker.CurrentActorPosition = ifMaxPlayer(gs.ActionTracker.CurrentActorPosition, len(gs.Players))
		log.Printf("%s (%s) calls %.0f.", gs.Players[gs.ActionTracker.CurrentActorPosition].Name, gs.Players[gs.ActionTracker.CurrentActorPosition].TypeOfPlayer, gs.ActionTracker.CurrentTotalBet)
	} else {
		log.Fatalf("Error, option returned %s", option)
	}
	return gs
}

func checkIfHandOverFromFolding(p []player.Player) (bool, int) {
	counter := 0
	var winnerIndex int
	for i := 0; i < len(p); i++ {
		if p[i].Folded == false {
			counter++
			winnerIndex = i
		}
	}
	if counter > 1 {
		return false, 0
	} else {
		return true, winnerIndex
	}
}

func ifMaxPlayer(one, two int) int {
	if one == (two - 1) {
		return 0
	} else {
		return one + 1
	}
}
